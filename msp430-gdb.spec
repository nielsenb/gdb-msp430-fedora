%global target msp430

Name:		%{target}-gdb
Version:	7.2a
# There has been no release, so this is a snapshot
Release:	20111205_1
Summary:	GDB for (remote) debugging MSP430 binaries
Group:		Development/Debuggers
License:	GPLv3+
URL:		http://mspgcc.sf.net/
Source0:	http://ftp.gnu.org/gnu/gdb/gdb-7.2a.tar.gz
# First patch is from mspgcc project
# http://sourceforge.net/projects/mspgcc/files/Patches/
# Second patch is dirty hack to prevent texinfo from barfing
# http://trac.cross-lfs.org/ticket/926
Patch0:		msp430-gdb-7.2a-20111205.patch
Patch1:		msp430-gdb-7.2a-docfix.patch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires: ncurses-devel%{?_isa} texinfo gettext flex bison
BuildRequires: expat-devel%{?_isa}
%if 0%{!?rhel:1} || 0%{?rhel} > 6
BuildRequires: xz-devel%{?_isa}
%endif
# dlopen() no longer makes rpm-libs%{?_isa} (it's .so) a mandatory dependency.
BuildRequires: zlib-devel%{?_isa} libselinux-devel%{?_isa}
# Permit rebuilding *.[0-9] files even if they are distributed in gdb-*.tar:
BuildRequires: /usr/bin/pod2man

%description
This is a version of GDB, the GNU Project debugger, for (remote)
debugging %{target} binaries. GDB allows you to see and modify what is
going on inside another program while it is executing.

%prep
%setup -q -c -n %{name}
# It extracts without the 'a' so we can't use the version
pushd gdb-7.2
%patch0 -p1
%patch1 -p1
popd

%build
mkdir -p build
cd build
CFLAGS="$RPM_OPT_FLAGS" ../gdb-7.2/configure --prefix=%{_prefix} \
	--libdir=%{_libdir} --mandir=%{_mandir} --infodir=%{_infodir} \
	--target=%{target} --without-python --disable-rpath --disable-nls --disable-werror
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
cd build
make install DESTDIR=$RPM_BUILD_ROOT

# we don't want these as this is a cross-compiler
rm -rf $RPM_BUILD_ROOT%{_infodir}
rm -f $RPM_BUILD_ROOT%{_libdir}/libiberty.a

# Don't ship the static library file:
rm -f $RPM_BUILD_ROOT%{_libdir}/libmsp430-sim.a

# Clean up unwanted syscalls
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/amd64-linux.xml
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/gdb-syscalls.dtd
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/i386-linux.xml
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/ppc-linux.xml
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/ppc64-linux.xml
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/sparc-linux.xml
rm -f $RPM_BUILD_ROOT/usr/share/gdb/syscalls/sparc64-linux.xml

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/%{target}-*
%{_mandir}/man1/%{target}-*.1.gz
%doc gdb-7.2/COPYING*

%changelog
* Fri Jul 11 2014 Brandon Nielsen <nielsenb@jetfuse.net> 7.2a-20111205-1
- Try to fix BuildRequires for building on Copr
- Add an additional identifier to release
* Tue Jul 08 2014 Brandon Nielsen <nielsenb@jetfuse.net> 7.2a-20111205
- Update to gdb 7.2a with version 20111205 of the mspgcc patch
- Add patch so documentation compiles under Fedora 20
- Disable rpaths to keep rpmbuild happy
- Compile without-python to prevent import error on startup
* Sun Jun 21 2009 Rob Spanton <rspanton@zepler.net> 6.8-1.20090621cvs
- Initial packaging
