===================
 gdb-msp430-fedora
===================

---------------------------------------------------------
Desiderata for compiling gdb for msp430 targets on Fedora
---------------------------------------------------------

Includes a specfile for building an RPM as well as a hacky patch inspired
from `elsewhere`_.

A compiled RPM can be installed from the `gdb-msp430-fedora copr repository`_.

.. _elsewhere: http://trac.cross-lfs.org/ticket/926
.. _gdb-msp430-fedora copr repository: https://copr.fedoraproject.org/coprs/nielsenb/gdb-msp430-fedora/
